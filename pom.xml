<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>at.apogeum.bitbucket</groupId>
    <artifactId>repodesc</artifactId>
    <version>2.5.0</version>

    <packaging>atlassian-plugin</packaging>

    <name>Bitbucket Server Repository Descriptions</name>
    <description>
        Adds repository descriptions, commit messages, and commit timestamps to the project repositories list.
        Repository descriptions can be added or edited by any repository admin in the settings area.  The commit data
        is extracted from each repository's most recent commit.
    </description>
    <organization>
        <name>Stephan Bechter</name>
        <url>https://plus.google.com/u/0/+StephanBechter</url>
    </organization>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.bitbucket.server</groupId>
                <artifactId>bitbucket-parent</artifactId>
                <version>${bitbucket.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.13</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jgit</groupId>
            <artifactId>org.eclipse.jgit</artifactId>
            <version>4.6.0.201612231935-r</version>
            <scope>compile</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>*</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.jcraft</groupId>
            <artifactId>jzlib</artifactId>
            <version>1.1.3</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <version>2.0.2</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-runtime</artifactId>
            <version>2.0.2</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
            <version>1</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-spi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <version>1.1.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.activeobjects</groupId>
            <artifactId>activeobjects-plugin</artifactId>
            <version>${ao.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.10</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>2.6</version>
            <scope>provided</scope>
        </dependency>
        <!-- WIRED TEST RUNNER DEPENDENCIES -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>1.1.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.2.2-atlassian-1</version>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <version>1.0.2</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.wink</groupId>
            <artifactId>wink-client</artifactId>
            <version>1.1.3-incubating</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.8.5</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.6.6</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.1.1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>bitbucket-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <products>
                        <!-- Node 1 -->
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket-node-1</instanceId>
                            <version>${bitbucket.version}</version>
                            <dataVersion>${bitbucket.data.version}</dataVersion>
                            <httpPort>7991</httpPort>
                            <systemPropertyVariables>
                                <bitbucket.shared.home>${project.build.directory}/bitbucket-node-1/home/shared</bitbucket.shared.home>
                                <!-- override the SSH port used for this node -->
                                <bitbucket.plugin.ssh.port>7997</bitbucket.plugin.ssh.port>
                                <!-- override database settings so both nodes use a single database -->
                                <jdbc.driver>com.mysql.jdbc.Driver</jdbc.driver>
                                <jdbc.url>jdbc:mysql://localhost:3306/bitbucket_repodesc?characterEncoding=utf8&amp;useUnicode=true&amp;sessionVariables=storage_engine%3DInnoDB</jdbc.url>
                                <jdbc.user>bitbucket</jdbc.user>
                                <jdbc.password>IAmStash</jdbc.password>
                                <!-- allow this node to find other nodes via TCP/IP -->
                                <hazelcast.network.tcpip>true</hazelcast.network.tcpip>
                                <!-- set to true if your load balancer supports stick sessions -->
                                <hazelcast.http.stickysessions>false</hazelcast.http.stickysessions>
                                <!-- forces Stash to fully finish starting up before yielding to the test runner or atlas-run -->
                                <johnson.spring.lifecycle.synchronousStartup>true</johnson.spring.lifecycle.synchronousStartup>
                            </systemPropertyVariables>
                            <libArtifacts>
                                <!-- ensure MySQL drivers are available to Stash -->
                                <libArtifact>
                                    <groupId>mysql</groupId>
                                    <artifactId>mysql-connector-java</artifactId>
                                    <version>5.1.32</version>
                                </libArtifact>
                            </libArtifacts>
                        </product>
                        <!-- Node 2 -->
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket-node-2</instanceId>
                            <version>${bitbucket.version}</version>
                            <dataVersion>${bitbucket.data.version}</dataVersion>
                            <httpPort>7992</httpPort>
                            <systemPropertyVariables>
                                <bitbucket.shared.home>${project.build.directory}/bitbucket-node-1/home/shared</bitbucket.shared.home>
                                <!-- override the SSH port used for this node -->
                                <bitbucket.plugin.ssh.port>7998</bitbucket.plugin.ssh.port>
                                <!-- override database settings so both nodes use a single database -->
                                <jdbc.driver>com.mysql.jdbc.Driver</jdbc.driver>
                                <jdbc.url>jdbc:mysql://localhost:3306/bitbucket_repodesc?characterEncoding=utf8&amp;useUnicode=true&amp;sessionVariables=storage_engine%3DInnoDB</jdbc.url>
                                <jdbc.user>bitbucket</jdbc.user>
                                <jdbc.password>IAmStash</jdbc.password>
                                <!-- allow this node to find other nodes via TCP/IP -->
                                <hazelcast.network.tcpip>true</hazelcast.network.tcpip>
                                <!-- set to true if your load balancer supports stick sessions -->
                                <hazelcast.http.stickysessions>false</hazelcast.http.stickysessions>
                                <!-- forces Stash to fully finish starting up before yielding to the test runner or atlas-run -->
                                <johnson.spring.lifecycle.synchronousStartup>true</johnson.spring.lifecycle.synchronousStartup>
                            </systemPropertyVariables>
                            <libArtifacts>
                                <!-- ensure MySQL drivers are available to Stash -->
                                <libArtifact>
                                    <groupId>mysql</groupId>
                                    <artifactId>mysql-connector-java</artifactId>
                                    <version>5.1.32</version>
                                </libArtifact>
                            </libArtifacts>
                        </product>
                    </products>
                    <testGroups>
                        <!-- tell AMPS / Maven which products ie nodes to run for the named testGroup 'clusterTestGroup' -->
                        <testGroup>
                            <id>clusterTestGroup</id>
                            <productIds>
                                <productId>bitbucket-node-1</productId>
                                <productId>bitbucket-node-2</productId>
                            </productIds>
                        </testGroup>
                    </testGroups>
                    <allowGoogleTracking>false</allowGoogleTracking>
                    <productDataPath>${basedir}/src/test/resources/generated-test-resources.zip</productDataPath>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>load-balancer-maven-plugin</artifactId>
                <version>1.1</version>
                <executions>
                    <execution>
                        <id>start-load-balancer</id>
                        <phase>pre-integration-test</phase>
                        <goals><goal>start</goal></goals>
                    </execution>
                    <execution>
                        <id>stop-load-balancer</id>
                        <phase>post-integration-test</phase>
                        <goals><goal>stop</goal></goals>
                    </execution>
                </executions>
                <configuration>
                    <balancers>
                        <balancer>
                            <port>7990</port>
                            <targets>
                                <target><port>7991</port></target>
                                <target><port>7992</port></target>
                            </targets>
                        </balancer>
                        <balancer>
                            <port>7999</port>
                            <targets>
                                <target><port>7997</port></target>
                                <target><port>7998</port></target>
                            </targets>
                        </balancer>
                    </balancers>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.6</source>
                    <target>1.6</target>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <properties>
        <bitbucket.version>5.6.3</bitbucket.version>
        <bitbucket.data.version>${bitbucket.version}</bitbucket.data.version>
        <amps.version>6.3.14</amps.version>
        <plugin.testrunner.version>1.2.0</plugin.testrunner.version>
        <ao.version>0.21.2</ao.version>
    </properties>

    <developers>
        <developer>
            <id>stb</id>
            <email>stephan@apogeum.at</email>
            <name>Stephan Bechter</name>
            <timezone>+1</timezone>
        </developer>
    </developers>
    <contributors>
        <contributor>
            <email>stephan@apogeum.at</email>
            <name>Stephan Bechter</name>
            <timezone>+1</timezone>
        </contributor>
    </contributors>
</project>
