package at.apogeum.bitbucket.repodesc.rest.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
@XmlRootElement(name = "repositories")
@XmlAccessorType(XmlAccessType.FIELD)
public class DescriptionsData {

    @XmlElement
    private List<RepositoryData> repositories = new ArrayList<RepositoryData>();

    @XmlElement
    private String jiraLink;

    public DescriptionsData() {
    }

    public void addRepoData(RepositoryData repositoryData) {
        repositories.add(repositoryData);
    }

    public void setJiraLink(String jiraLink) {
        this.jiraLink = jiraLink;
    }

    public String getJiraLink() {
        return jiraLink;
    }

    public List<RepositoryData> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<RepositoryData> repositories) {
        this.repositories = repositories;
    }
}