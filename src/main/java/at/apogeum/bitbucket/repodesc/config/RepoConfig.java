package at.apogeum.bitbucket.repodesc.config;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Default;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;
import net.java.ao.schema.Unique;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
@Table("RepoConfig001")
@Preload
public interface RepoConfig extends Entity{

    @NotNull
    @Unique
    public Integer getRepoId();

    @NotNull
    @Default("none")
    public String getProjectKey();

    public void setProjectKey(String projectKey);

    @NotNull
    @Default("none")
    public String getSlug();

    public void setSlug(String slug);

    @StringLength(value=StringLength.UNLIMITED)
    public String getDescription();

    public void setDescription(String description);
}
