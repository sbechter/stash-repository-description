package at.apogeum.bitbucket.repodesc.servlet;

import at.apogeum.bitbucket.repodesc.JGitUtil;
import at.apogeum.bitbucket.repodesc.config.RepoConfig;
import at.apogeum.bitbucket.repodesc.config.RepoConfigPersistenceManager;
import com.atlassian.bitbucket.AuthorisationException;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.BooleanUtils;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
public class RepositoryDataServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(RepositoryDataServlet.class);

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PageBuilderService pageBuilderService;
    private final RepositoryService repositoryService;
    private final PermissionValidationService permissionValidationService;
    private final RepoConfigPersistenceManager repoConfigPersistenceManager;

    public RepositoryDataServlet(SoyTemplateRenderer soyTemplateRenderer,
                    PageBuilderService pageBuilderService, RepositoryService repositoryService,
                    PermissionValidationService permissionValidationService,
                    RepoConfigPersistenceManager repoConfigPersistenceManager) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.pageBuilderService = pageBuilderService;
        this.repositoryService = repositoryService;
        this.permissionValidationService = permissionValidationService;
        this.repoConfigPersistenceManager = repoConfigPersistenceManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Repository repository = getRepository(req);

        if (repository == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        renderInternal(resp, repository, null, null);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Repository repository = getRepository(req);
        if (repository == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        try {
            permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);
        } catch (AuthorisationException ex) {
            renderInternal(resp, repository, null, Boolean.FALSE);
            return;
        }

        String description = req.getParameter("description");
        try {
            repoConfigPersistenceManager.setRepositoryConfigurationForRepository(repository, description);
        } catch (SQLException e) {
            resp.sendError(500, e.getMessage());
        }
        LOG.info("Saving repository description fore repository [" + repository.getName() + "] : " + description);
        renderInternal(resp, repository, description, Boolean.TRUE);
    }

    protected void render(HttpServletResponse resp, Map<String, Object> data)
                    throws IOException, ServletException {
        pageBuilderService.assembler().resources().requireWebResource("at.apogeum.bitbucket.repodesc:repodesc-resources");

        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(),
                            "at.apogeum.stash.repodesc:repodesc-soy",
                            "plugin.repodesc.settings",
                            data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

    private void renderInternal(HttpServletResponse resp, Repository repository, String description, Boolean status)
                    throws IOException, ServletException {

        if (description == null) {
            description = "-";
            try {
                RepoConfig config = repoConfigPersistenceManager.getRepositoryConfigurationForRepository(repository);
                description = config.getDescription();
            } catch (SQLException e) {
                resp.sendError(500, e.getMessage());
            }
        }
        // config.getDescription() can return null - change it to a dash.
        if (description == null) {
            description = "-";
        }

        ImmutableMap<String, Object> payload = ImmutableMap.<String, Object>of(
                        "repository", repository,
                        "description", description,
                        "success", BooleanUtils.toBoolean(status),
                        "failure", BooleanUtils.toBoolean(status));

        render(resp, payload);
    }

    private Repository getRepository(HttpServletRequest request) throws IOException {
        String pathInfo = request.getPathInfo();
        String[] parameters = pathInfo.split("/");
        pathInfo = pathInfo.startsWith("/") ? pathInfo.substring(0) : pathInfo;
        String[] pathParts = pathInfo.split("/");
        if (pathParts.length != 3) {
            return null;
        }
        return repositoryService.getBySlug(pathParts[1], pathParts[2]);
    }

}