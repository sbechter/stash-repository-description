define('plugin/repodesc/default', [
    'jquery',
    'bitbucket/util/navbuilder'
], function ($, nav) {

    // http://stackoverflow.com/questions/9229645/remove-duplicates-from-javascript-array/9229821
    function uniq(a) {
        return a.sort().filter(function (item, pos, ary) {
            return !pos || item != ary[pos - 1];
        })
    }

    function extractIds(s) {
        function reverse(s) {
            for (var i = s.length - 1, o = ''; i >= 0; o += s[i--]) {
            }
            return o;
        }

        var jira_matcher = /\d+-[A-Z]+(?!-?[a-zA-Z]{1,10})/g;
        var r = reverse(s);
        var matches = r.match(jira_matcher);
        if (!matches) {
            matches = []
        }
        for (var j = 0; j < matches.length; j++) {
            var m = reverse(matches[j]);
            matches[j] = m.replace(/-0+/, '-'); // trim leading zeroes:  ABC-0123 becomes ABC-123
        }

        // need to remove duplicates, since they will cause n^2 links to be created (n = dups).
        return uniq(matches);
    }

    var dateOptionsLong = {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'};

    function toDate(str) {
        var val = parseInt(str, 10);
        if (!isNaN(val)) {
            var time = new Date(val);
            return '<time datetime="' + time.toISOString() + '" data-unixtime="' + str + '">' + time.toLocaleDateString(undefined, dateOptionsLong) + '</time>';
        }
        return '';
    }

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        } else {
            return results[1] || 0;
        }
    };


    function loadMetadata() {
        var $repoTable = ($("#repositories-container table").length ? $("#repositories-container table") : $("#repository-container table"));
        if (!$repoTable.length) {
            // try to get table for BBServer prior 5.5
            $repoTable = $("#repositories-table");
        }
        if ($repoTable.length) {
            var projectSlug = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
            var userSlug = $(".aui-avatar.aui-avatar-xxlarge.user-avatar").attr("data-username");
            var visibilityParam = $.urlParam('visibility');
            var dataUrl = nav.rest('repodesc').build();
            if (userSlug && (projectSlug === 'profile' || projectSlug === userSlug)) {
                // projectSlug === 'profile' > 5.6.3, before same as userSlug
                dataUrl += "/private/" + userSlug;
            } else {
                if (visibilityParam === "public") {
                    dataUrl += "/public";
                } else {
                    dataUrl += "/projects/" + projectSlug;
                }
            }
            $.ajax({
                url: dataUrl,
                type: "GET",
                dataType: "json",
                success: function(response) {

                    $repoTable.addClass("aui-table-sortable");
                    $repoTable.find("thead tr:nth-child(1)").append("<th>" + AJS.I18n.getText(
                            "repodesc.description.label") + "</th>"
                            + "<th>" + AJS.I18n.getText("repodesc.mostrecentcommit.label") + "</th>"
                            + "<th>" + AJS.I18n.getText("repodesc.author.label") + "</th>"
                            + "<th>" + AJS.I18n.getText("repodesc.date.label") + "</th>");
                    var index = 1;
                    response.repositories.forEach(function(repoData) {
                        var jira = response.jiraLink;
                        var msg = repoData.lastUpdateMessage;
                        var ids = extractIds(msg);
                        if (jira) {
                            for (var j = 0; ids && ids.length && j < ids.length; j++) {
                                id = ids[j];
                                msg = msg.replace(
                                        new RegExp(id, 'g'),
                                        "<a class='jira-issues-trigger' data-single-issue='true' data-issue-keys='" + id
                                        + "' href='" + jira + "/browse/" + id + "'>" + id + "</a>");
                            }
                        }
                        var html = "<td class='repodesc' title='" + repoData.descriptionFull + "'>"
                                + repoData.description + "</td>";
                        html += "<td class='repodesc repodesc_commit' title='" + repoData.lastUpdateMessageFull + "'>"
                                + (msg ? msg: "&nbsp") + "</td>";
                        html += "<td class='repodesc_commit'>" + (repoData.lastAuthor ? repoData.lastAuthor : "&nbsp;") + "</td>";
                        html += "<td class='repodesc_commit'>" + (repoData.lastUpdateTime ? toDate(repoData.lastUpdateTime) : "&nbsp;") + "</td>";
                        $repoTable.find("tbody tr:nth-child(" + index + ")").append(html);
                        ++index;
                    });

                    AJS.$('td.repodesc').tooltip({delayIn: 200});
                    $("#repositories-table").tablesorter({
                        textExtraction: function(node) {
                            var nft = (node.firstChild && node.firstChild.tagName) || undefined;
                            if (nft && nft.toUpperCase() === 'TIME') {
                                return node.firstChild.getAttribute("data-unixtime");
                            } else {
                                return $(node).text();
                            }
                        }
                    });
                    AJS.tablessortable.setTableSortable(AJS.$("#repositories-table"));

                    require(['bitbucket/internal/util/ajax'], function(ajax) {
                        jiraIntegration.triggers.init({
                            id: 'commits-jira-issues-dialog',
                            selector: '.jira-issues-trigger'
                        }, {
                            ajax: function(options) {
                                return ajax.rest($.extend({
                                    statusCode: {
                                        200: false,
                                        500: false
                                    }
                                }, options));
                            }
                        });
                    });
                }
            });
        }
    }

    $(document).ajaxSuccess(
        function(event, xhr, settings) {
            if (settings.url.indexOf('rest/api/latest/projects') !== 0 && xhr.status === 200 && xhr.responseJSON) {
                const response = xhr.responseJSON;
                if (response.isLastPage) {
                    //console.log('Loading metadata');
                    loadMetadata()
                }
            }
        }
   );

});

jQuery(function () {
    require('plugin/repodesc/default');
});
